#!/usr/bin/python3

import sys, logging, bluetooth, json

script_path = '/home/pi/meteo/python/'

with open(script_path + 'config.json') as config_file:
    meteo_config = json.load(config_file)

FORMAT = '[%(asctime)s] %(levelname)s: %(message)s в строке %(lineno)d (%(filename)s)'
logging.basicConfig(format=FORMAT, level=logging.INFO, filename=script_path + 'log.txt')

bd_addr = meteo_config['bd_addr']
port = 1

try:
    bsocket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    bsocket.connect((bd_addr, port))
    bsocket.setblocking(False)
except bluetooth.BluetoothError as e:
    logging.error(e)
    sys.exit(0)


from plotly import graph_objs as go, plotly as py

py.sign_in(meteo_config['plotly_username'], meteo_config['plotly_api_key'])
token_1 = meteo_config['plotly_streaming_tokens'][0]
token_2 = meteo_config['plotly_streaming_tokens'][1]
token_3 = meteo_config['plotly_streaming_tokens'][2]
token_4 = meteo_config['plotly_streaming_tokens'][3]

stream_id1 = dict(token=token_1, maxpoints=100)
stream_id2 = dict(token=token_2, maxpoints=100)
stream_id3 = dict(token=token_3, maxpoints=100)
stream_id4 = dict(token=token_4, maxpoints=100)

trace1 = go.Scatter(x=[], y=[], stream=stream_id1, name='Температура снаружи')
trace2 = go.Scatter(x=[], y=[], stream=stream_id2, name='Температура внутри',
                    marker=dict(color='rgb(148, 103, 189)'))
trace3 = go.Scatter(x=[], y=[], stream=stream_id3, name='Давление')
trace4 = go.Scatter(x=[], y=[], stream=stream_id4, name='Влажность')

data1 = [trace1, trace2]
layout1 = go.Layout(
    title='Температуры',
    xaxis=dict(
        title='Метка времени'
        ),
    yaxis=dict(
        title='Температура, ˚C'
    )
)
data2 = [trace3]
layout2 = go.Layout(
    title='Давление',
    xaxis=dict(
        title='Метка времени'
        ),
    yaxis=dict(
        title='Давление, мм рт. ст.'
    )
)
data3 = [trace4]
layout3 = go.Layout(
    title='Влажность',
    xaxis=dict(
        title='Метка времени'
        ),
    yaxis=dict(
        title='Влажность, %'
    ) 
)

fig1 = go.Figure(data=data1, layout=layout1)
plot_url1 = py.plot(fig1, filename='temperatures')
fig2 = go.Figure(data=data2, layout=layout2)
plot_url2 = py.plot(fig2, filename='pressure')
fig3 = go.Figure(data=data3, layout=layout3)
plot_url3 = py.plot(fig3, filename='humidity')

s_1 = py.Stream(stream_id=token_1)
s_2 = py.Stream(stream_id=token_2)
s_3 = py.Stream(stream_id=token_3)
s_4 = py.Stream(stream_id=token_4)

s_1.open()
s_2.open()
s_3.open()
s_4.open()

graphs = [
    meteo_config['plotly_graphs'][0],
    meteo_config['plotly_graphs'][1],
    meteo_config['plotly_graphs'][2]
]

template = (''
            '<a href="{graph_url}" target="_blank">'  # Open the interactive graph when you click on the image
            '<img src="{graph_url}.png">'  # Use the ".png" magic url so that the latest, most-up-to-date image is included
            '</a>'
            '{caption}'  # Optional caption to include below the graph
            '<br>'  # Line break
            '<a href="{graph_url}" style="color: rgb(190,190,190); text-decoration: none; font-weight: 200;" target="_blank">'
            'Нажми для перехода к диаграмме на Plotly'  # Direct readers to Plotly for commenting, interactive graph
            '</a>'
            '<br>'
            '<hr>'  # horizontal line
            '')

from sqlalchemy import create_engine
from sqlalchemy import Table, Column, text, String, MetaData, DateTime, select, between

engine = create_engine('sqlite:///' + script_path + 'meteo.db', echo=False)

metadata = MetaData()

tout = Table('tout', metadata,
            Column('Timestamp', DateTime, server_default=text('CURRENT_TIMESTAMP')),
            Column('Value', String)
            )
tin = Table('tin', metadata,
            Column('Timestamp', DateTime, server_default=text('CURRENT_TIMESTAMP')),
            Column('Value', String)
            )
pressure = Table('pressure', metadata,
            Column('Timestamp', DateTime, server_default=text('CURRENT_TIMESTAMP')),
            Column('Value', String)
            )
humidity = Table('humidity', metadata,
            Column('Timestamp', DateTime, server_default=text('CURRENT_TIMESTAMP')),
            Column('Value', String)
            )

metadata.create_all(engine)

from IPython.display import display, HTML
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

me = meteo_config['me']
recipient = meteo_config['recipient']
subject = 'Диаграммы'

email_server_host = meteo_config['server_host']
port = int(meteo_config['port'])
email_username = me
email_password = meteo_config['password']

msg = MIMEMultipart('alternative')
msg['From'] = me
msg['To'] = recipient
msg['Subject'] = subject

import pandas as pd, time as tm, pytz
from datetime import datetime as dt, date, time, timedelta

email_hour = 8
size = 57
utc_offset = int(tm.localtime().tm_gmtoff / 3600)

now = dt.now()
while now.minute % 5 != 0:
    now = dt.now()

while True:
    try:
        try:
            tm.sleep(5)
            data = ''
            while len(data) < size:
                d = bsocket.recv(size - len(data)).decode('utf-8').strip()
                data += d
        except:
            continue

        if not data.startswith('{') and not data.endswith('}'):
            continue
        
        print(data)
        # print(len(data))
        
        try:
            df = pd.read_json(data, lines=True)
            # print(df)
        except Exception as e:
            logging.error(e)
            continue
        
        y1 = float(df.tout[0])
        y2 = float(df.tin[0])
        y3 = round(float(df.p[0]))
        y4 = round(float(df.h[0]))

        now = dt.now()
        x = now.strftime('%Y-%m-%d %H:%M:%S')
        
        print(x)
        print(y1,y2,y3,y4)

        try:
            s_1.write(dict(x=x, y=y1))
            s_2.write(dict(x=x, y=y2))
            s_3.write(dict(x=x, y=y3))
            s_4.write(dict(x=x, y=y4))
        except Exception as e:
            logging.error(e)
            tm.sleep(300)
            continue    
            
        if now.minute in (0,1,2,3,4):

            try:
                ins = tout.insert().values(Value=str(y1))
                conn = engine.connect()
                result = conn.execute(ins)
                ins = tin.insert().values(Value=str(y2))
                conn = engine.connect()
                result = conn.execute(ins)
                ins = pressure.insert().values(Value=str(y3))
                conn = engine.connect()
                result = conn.execute(ins)
                ins = humidity.insert().values(Value=str(y4))
                conn = engine.connect()
                result = conn.execute(ins)
            except Exception as e:
                logging.error(e)
                tm.sleep(300)
                continue
                
            if now.hour == email_hour:

                try:
                    today = date.today()
                    d1 = dt.combine(today, time(email_hour,00,00)) - timedelta(days=1) - timedelta(hours=utc_offset)
                    d2 = dt.combine(today, time(email_hour,00,00)) - timedelta(hours=utc_offset)

                    df1 = pd.read_sql_query(select([tout]).where(tout.c.Timestamp.between(d1, d2)), engine)
                    df2 = pd.read_sql_query(select([tin]).where(tin.c.Timestamp.between(d1, d2)), engine)
                    df3 = pd.read_sql_query(select([pressure]).where(pressure.c.Timestamp.between(d1, d2)), engine)
                    df4 = pd.read_sql_query(select([humidity]).where(humidity.c.Timestamp.between(d2, d2)), engine)

                    df1.Timestamp = pd.to_datetime(df1.Timestamp)
                    df2.Timestamp = pd.to_datetime(df2.Timestamp)
                    df3.Timestamp = pd.to_datetime(df3.Timestamp)
                    df4.Timestamp = pd.to_datetime(df4.Timestamp)
                    
                    data4 = [
                        go.Scatter(
                            x=df1.Timestamp + pd.Timedelta(str(utc_offset) + ' hours'),
                            y=df1.Value,
                            name='Температура снаружи'
                        ),
                        go.Scatter(
                            x=df2.Timestamp + pd.Timedelta(str(utc_offset) + ' hours'),
                            y=df2.Value,
                            name='Температура внутри',
                            marker=dict(color='rgb(148, 103, 189)')
                        )
                    ]
                    data5 = [
                        go.Scatter(
                            x=df3.Timestamp + pd.Timedelta(str(utc_offset) + ' hours'),
                            y=df3.Value,
                            name='Давление'
                        )
                    ]
                    data6 = [
                        go.Scatter(
                            x=df4.Timestamp + pd.Timedelta(str(utc_offset) + ' hours'),
                            y=df4.Value,
                            name='Влажность'
                        )
                    ]

                    fig4 = go.Figure(data=data4, layout=layout1)
                    plot_url4 = py.plot(fig4, filename='e_temperatures')
                    fig5 = go.Figure(data=data5, layout=layout2)
                    plot_url5 = py.plot(fig5, filename='e_pressure')
                    fig6 = go.Figure(data=data6, layout=layout3)
                    plot_url6 = py.plot(fig6, filename='e_humidity')
                        
                    #Sending the email

                    email_body = ''
                    for graph in graphs:
                        _ = template
                        _ = _.format(graph_url=graph, caption='Meteo')
                        email_body += _

                    display(HTML(email_body))
                    msg.attach(MIMEText(email_body, 'html'))
                    
                    server = smtplib.SMTP(email_server_host, port)
                    server.ehlo()
                    server.starttls()        
                    server.login(email_username, email_password)
                    server.sendmail(me, recipient, msg.as_string())
                    server.close()
                except Exception as e:
                    logging.error(e)
                    tm.sleep(300)
                    continue

        tm.sleep(300)
        
    except Exception as e:
        logging.error(e)
        continue

    except KeyboardInterrupt:
        bsocket.close()
        s_1.close()
        s_2.close()
        s_3.close()
        s_4.close()
        sys.exit(0)
