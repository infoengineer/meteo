#include <OneWire.h>
#include <DallasTemperature.h>

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "DHT.h"
#include <Adafruit_BMP085.h>

#define ONE_WIRE_BUS 10

#define DHTPIN 2     // what digital pin we're connected to

#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Connect VCC of the BMP085 sensor to 3.3V (NOT 5.0V!)
// Connect GND to Ground
// Connect SCL to i2c clock - on '168/'328 Arduino Uno/Duemilanove/etc thats Analog 5
// Connect SDA to i2c data - on '168/'328 Arduino Uno/Duemilanove/etc thats Analog 4
// EOC is not used, it signifies an end of conversion
// XCLR is a reset pin, also not used here

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

DHT dht(DHTPIN, DHTTYPE);

Adafruit_BMP085 bmp;

void setup() {
  
  Serial.begin(9600);
  sensors.begin();
  dht.begin();
  bmp.begin(); 
  
}
  
void loop() {

  sensors.requestTemperatures(); // Send the command to get temperatures
  float tout = sensors.getTempCByIndex(0);

  // Wait a few seconds between measurements.
  delay(10000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  float p = bmp.readPressure();
  float tin = bmp.readTemperature();

  String json_str = "{\"tout\":\"";
  if (tout >= 0) {json_str += "+";} else {json_str += "-";}
  if (abs(tout) >= 0 && abs(tout) < 10) {json_str += "0";}
  json_str += String(abs(tout)) +"\"";
  json_str += ",\"tin\":\"";
  if (tin >= 0) {json_str += "+";} else {json_str += "-";}
  if (abs(tin) >= 0 && abs(tin) < 10) {json_str += "0";}
  json_str += String(abs(tin)) + "\""; 
  json_str += ",\"p\":\"";
  json_str += String(p/133.32) + "\"";
  json_str += ",\"h\":\"";
  json_str += String(h) + "\"";
  json_str += "}";
  Serial.println(json_str);
  delay(50000);
  
}
